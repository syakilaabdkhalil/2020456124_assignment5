function myFunction() {
  	document.getElementById("Form").reset();
  		alert ("The inputs will be reset");
}

 function calculate(){

 	var name = document.forms ["Form"]["name"].value;
 	var length = document.forms ["Form"]["length"].value; 
	var width = document.forms ["Form"]["width"].value;
	var height = document.forms ["Form"]["height"].value;
	var mode = document.forms ["Form"]["mode"].value;
	var type = document.forms ["Form"]["type"].value;

	var weight = (length*width*height)/5000;

	
	if(document.getElementById('surface').checked){
		mode="Surface";
	}

	if(document.getElementById('air').checked){
		mode="Air";
	}

	if(document.getElementById('international').checked){
		type="International";
	}

	if(document.getElementById('domestic').checked){
		type="Domestic";
	}

	alert ("Parcel Volumetric and Cost Calculator" + "\nName: " +name + "\nLength: " +length +"cm"
		+ "\nWidth: "+width+"cm"+ "\nHeight:"+height+"cm" + "\nWeight: " +weight+"kg"
		+ "\nMode: "+mode+ "\nType: "+type +"\nDelivery cost: RM " +getCost(mode, type, weight) )

 }


 function getCost(mode, type, weight){
 	var cost;

 	if (document.getElementById('domestic').checked){
 		if(document.getElementById('surface').checked){
 			if(weight<2){
 				cost= 7.00;
 			}
 			else{
 				cost=7.00 + (weight-2)*1.50;
 			}
 		}
 		else{
 			if(weight<2){
 				cost= 10.00;
 			}
 			else{
 				cost=10.00 + (weight-2)*3.00;
 			}
 		}	
 	}
 	else{
 		if(document.getElementById('surface').checked){
 			if(weight<2){
 				cost= 20.00;
 			}
 			else{
 				cost=20.00 + (weight-2)*3.00;
 			}
 		}
 		else{
 			if(weight<2){
 				cost= 50.00;
 			}
 			else{
 				cost=50.00 + (weight-2)*5.00;
 			}
 		}		
 	}
 	return cost;
 }
